import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { VistaAppsComponent } from './vista-apps.component';

describe('VistaAppsComponent', () => {
  let component: VistaAppsComponent;
  let fixture: ComponentFixture<VistaAppsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VistaAppsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(VistaAppsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
