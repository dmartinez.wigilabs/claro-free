import { Component, OnInit } from '@angular/core';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { ApiService } from 'src/app/api.service';
import { Router } from '@angular/router';
import { UserService } from '../userService/user.service'

@Component({
  selector: 'app-inicio',
  templateUrl: './inicio.component.html',
  styleUrls: ['./inicio.component.css']
})
export class InicioComponent implements OnInit {
  LogForm: FormGroup;
  submitted = false;
  typeInput: boolean;
  correoValidate = 
  /^(([^<>()[\]\.,;:\s@\"]+(\.[^<>()[\]\.,;:\s@\"]+)*)|(\".+\"))@(([^<>()[\]\.,;:\s@\"]+\.)+[^<>()[\]\.,;:\s@\"]{2,})$/i;
  public UrlRouter = '/inicio';
  constructor(private formBuilder: FormBuilder, private apiService : ApiService, private router : Router, private userservice:UserService) { }

  ngOnInit() {
    this.LogForm = this.formBuilder.group({
      correo:['',[Validators.required, Validators.email, Validators.pattern(this.correoValidate)]],
      password:['',Validators.required]
    })

  }
  addType() {
    this.typeInput = !this.typeInput;
  }
  
  get f(){ return this.LogForm.controls}
  onSubmit(){
    this.submitted = true;
    if(this.LogForm.invalid){
      return
    }else{
      console.log(this.LogForm.value);
      this.router.navigateByUrl('/dashboard/app/Beat');
      /*this.apiService.loginUsuarios(this.f.correo.value, this.f.password.value).subscribe(data => {
        if(data.error == 1){
          alert('error');
        } else {
          this.router.navigateByUrl('/dashboard');
        }
      })*/
    }
  }
}
