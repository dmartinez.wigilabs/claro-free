import { Component, OnInit } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ApiService } from '../api.service';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { NgxSpinnerService } from 'ngx-spinner';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { DatePipe } from '@angular/common';

declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');

Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);

@Component({
  selector: 'app-rappi',
  templateUrl: './rappi.component.html',
  styleUrls: ['./rappi.component.css']
})
export class RappiComponent implements OnInit {
  public opciones = ['100','500','1000','5000']
  Datos: any = [];
  lat: number = 4.698830;
  lng: number = -74.040987;
  FeFormRappi: FormGroup;
  submitted = false;
  public max = new Date();
  public options: any = {
    chart: {
      // Editar chart espacio
      spacingBottom: 10,
      spacingTop: 20,
      spacingLeft: 20,
      spacingRight: 20,
    },
    title: {
      text: "Usuarios periodo",
      align: 'left',
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    xAxis:{
      categories:["1", "5", "10", "15", "20", "25","30"]
    },
    yAxis: {
      min: 0,
      max: 100,
      tickInterval: 20,
      title: {
          text: ''
      },
      labels: {
        formatter: function () {
            return  this.axis.defaultLabelFormatter.call(this) + '%';
        }            
      }
    },
    plotOptions: {
      series: {
        color: '#E93030'
      }
    },
    tooltip: {
      valueSuffix:"%"
    },
    series: [
       {
        name: 'usuario',
        data: [40, 60, 75, 30, 80, 40, 75]
       },
    ]
  } 
  public bar: any = {
    
    chart:{
      type: 'column',
      spacingBottom: 0,
      spacingTop: 15,
      spacingLeft: 15,
      spacingRight: 15,
    },
    title: {
      text: "Clics App/Hora",
      align: 'left',
    },
    plotOptions: {
      column: {
        pointPadding: 0,
        borderWidth: 0,
        groupPadding: 0,
        shadow: false
      },
      series: {
        pointWidth: 35,
        color: '#E93030'
      }
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    xAxis:{
      categories:["6 am -8am", "9 am -1pm", "2 pm -6pm", "7 pm -12am"]
    },
    yAxis: {
      min: 0,
      title: {
        text: ''
      },
    },
    series: [
      {
        name: 'click',
        data: [50, 700, 350, 1000]
      },
    ]
  }
  constructor(private apiService : ApiService,  private formBuilder: FormBuilder, private spinner: NgxSpinnerService,  private datePipe: DatePipe, dateTimeAdapter: DateTimeAdapter<any>) { }

  ngOnInit() {

    this.options =Highcharts.chart('container3', this.options); 
    this.bar = Highcharts.chart('container4', this.bar);
    this.Datos = [];
    this.apiService.getFechas().subscribe((res: any) =>{
      console.log(res)
      this.Datos = res;
    });
    this.FeFormRappi = this.formBuilder.group({
      desde:['',[
          Validators.required
      ]],
      hasta: ['',[
          Validators.required
      ]], 
    })
  }

  get f(){ return this.FeFormRappi.controls }
  onSubmit(){

    this.submitted = true;

    if(this.FeFormRappi.invalid){
        return;
    }else{
      alert('exit');
      console.log(this.FeFormRappi.value);
    }
    this.FeFormRappi.reset();
    this.submitted = false; 
  }
}
