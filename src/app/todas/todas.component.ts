import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as Highcharts from 'highcharts';
import { ApiService } from '../api.service';
import { OwlDateTimeModule, OwlNativeDateTimeModule, OwlDateTimeIntl} from 'ng-pick-datetime';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { formatDate } from '@angular/common';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import MapModule from 'highcharts/modules/map';





declare var require: any;
let Boost = require('highcharts/modules/boost');
let noData = require('highcharts/modules/no-data-to-display');
let More = require('highcharts/highcharts-more');


Boost(Highcharts);
noData(Highcharts);
More(Highcharts);
noData(Highcharts);


@Component({
  selector: 'app-todas',
  templateUrl: './todas.component.html',
  styleUrls: ['./todas.component.css']
})

export class TodasComponent  implements OnInit {
  public opciones = [
    [Date.UTC(2019, 8, 19), 100], 
    [Date.UTC(2019, 8, 20), 90],
    [Date.UTC(2019, 8, 21), 67],
    [Date.UTC(2019, 8, 22), 80],
    [Date.UTC(2019, 8, 23), 100], 
    [Date.UTC(2019, 8, 24), 90],
    [Date.UTC(2019, 8, 25), 67],
    [Date.UTC(2019, 8, 26), 80],
    [Date.UTC(2019, 8, 27), 100], 
    [Date.UTC(2019, 8, 28), 90],
    [Date.UTC(2019, 8, 29), 67],
    [Date.UTC(2019, 8, 30), 80],
    [Date.UTC(2019, 9, 1), 90],
    [Date.UTC(2019, 9, 2), 5],
    [Date.UTC(2019, 9, 3), 80],
    [Date.UTC(2019, 9, 4), 90],
    [Date.UTC(2019, 9, 5), 67],
    [Date.UTC(2019, 9, 6), 80],
    [Date.UTC(2019, 9, 7), 90],
    [Date.UTC(2019, 9, 8), 67],
    [Date.UTC(2019, 9, 9), 80],
    [Date.UTC(2019, 9, 10), 80],
    [Date.UTC(2019, 9, 11), 90],
    [Date.UTC(2019, 9, 12), 67],
    [Date.UTC(2019, 9, 13), 80],
  ]
  public Copiafechas
  public opcionesT = [
    ['Rappi', 850],
    ['Facebook', 1420],
    ['Instagram',2111],
    ['Easy taxy',2358]
  ]
  public opcionesTSP = [
    [Date.UTC(2019, 8, 19, 8, 10), 34.8], 
    [Date.UTC(2019, 8, 19, 8, 20), 73.9],
    [Date.UTC(2019, 8, 19, 8, 30), 99.9],
    [Date.UTC(2019, 8, 19, 8, 40), 10.9],
    [Date.UTC(2019, 8, 19, 8, 50), 34.8], 
    [Date.UTC(2019, 8, 19, 9, 0), 73.9],
    [Date.UTC(2019, 8, 19, 9, 10), 99.9],
    [Date.UTC(2019, 8, 19, 9, 20), 10.9],
  ]
  Datos: any = [];
  Datos1:any = [];
  Datos2:any = [];
  lat: number = 4.698830;
  lng: number = -74.040987;
  FeForm: FormGroup;
  submitted = false;
  public fechadesde: any;
  public fechahasta:any;
  public myDate = new Date();
  public fechaAc = new Date().getTime();
  
  public options: any = {
    chart: {
      //Editar chart espacio
      spacingBottom: 25,
      spacingTop: 25,
      spacingLeft: 25,
      spacingRight: 25,
    },
    title: {
      text: "Usuarios periodo",
      align: 'left',
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    xAxis:{
      type: 'datetime',
      tickInterval: 24 * 3600 * 1000,
      labels: {
        format: '{value:%e.%b}'
      },
    },
    yAxis: {
      min: 0,
      max: 100,
      tickInterval: 20,
      title: {
        text: ''
      },
      labels: {
        formatter: function () {
          return  this.axis.defaultLabelFormatter.call(this) + '%';
        }            
      }
    },
    plotOptions: {
      series: {
        color: '#E93030'
      }
    },
    tooltip: {
      valueSuffix:"%"
    },
    series: [
      {
        name: 'usuario',
        data: [[ this.fechaAc, 99]],
      },
    ]
  } 
  public spline: any = {
    chart:{
      type: 'spline',
      spacingBottom: 0,
      spacingTop: 0,
      spacingLeft: 15,
      spacingRight: 15,
    },
    title: {
      text: "Clics App/Hora",
      align: 'left',
    },
    plotOptions: {
      column: {
        pointPadding: 0,
        borderWidth: 0,
        groupPadding: 0,
        shadow: false
      },
      series: {
        pointWidth: 35,
        color: '#E93030'
      }
    },
    credits: {
      enabled: false
    },
    legend: {
      enabled: false
    },
    xAxis:{
      type: 'datetime'
    },
    yAxis: {
      min: 0,
      title: {
        text: ''
      },
    },
    series: [
      {
        name: 'click',
        data: [
          [Date.UTC(2019, 8, 19, 8, 15), 14.8], 
          [Date.UTC(2019, 8, 19, 8, 20), 43.9],
          [Date.UTC(2019, 8, 19, 8, 25), 39.9],
          [Date.UTC(2019, 8, 19, 8, 30), 40.9],
        ],
      },
    ]
  }
  public torta: any = {
    chart:{
      plotBorderWidth: null,
      plotShadow: false,
      spacingBottom: 0,
      spacingTop: 0,
      spacingLeft: 15,
      spacingRight: 15,
    },
    title: {
      text: "Clic por aplicación",
      align: 'left',
    },
    credits: {
      enabled: false
    },
    legend: {
      align: 'left',
      layout: 'vertical',
      verticalAlign: 'center',
      x: 0,
      y: 70,
      useHTML: true,
    },
    plotOptions : {
      pie: {
        allowPointSelect: true,
        cursor: 'pointer',
        center: [250, 50],
          dataLabels: {
            enabled: false           
          },
        showInLegend: true
      },
    },
    series: [{
      type: 'pie',
      name: 'clicks',
      data: [
        ['Rappi', 1850],
        ['Facebook', 420],
        ['Instagram',111],
        ['Easy taxy',58]
      ]  
    }],
    responsive: {
      rules: [{
        condition: {
            maxWidth: 375
        },
        chartOptions: {
          plotOptions: {
            pie: {
              allowPointSelect: true,
              cursor: 'pointer',
              center: [185, 50],
              showInLegend: true
            },
          },
          legend: {
            align: 'left',
            verticalAlign: 'center',
            layout: 'vertical'
          }
        },
      }],
    }
  }
  public mapa: any = {
    chart: {
      map: 'co/co-all'
    },
  }

  constructor(private apiService : ApiService, private formBuilder: FormBuilder, private datePipe: DatePipe, dateTimeAdapter: DateTimeAdapter<any>,
  private spinner: NgxSpinnerService) 
  { this.GetInfo() }
  
  ngOnInit() {
    this.options = Highcharts.chart('container', this.options); 
    this.spline = Highcharts.chart('container1', this.spline);
    this.torta = Highcharts.chart('container2', this.torta);
    this.mapa = Highcharts.chart('container3', this.mapa);
    console.log(this.fechaAc);

    this.FeForm = this.formBuilder.group({
      desde:['',[
        Validators.required
      ]],
      hasta: ['',[
        Validators.required
      ]], 
    })
  }

  GetInfo(){
   
  }

  get f(){ return this.FeForm.controls }

  onSubmit(){
    this.submitted = true;
    
    if(this.FeForm.invalid){
      return;
    }else{
      
      /*this.apiService.postFechas(this.FeForm.value).subscribe(
        (data) => {
          console.log(data);
          if (data){

          }else{
            alert('error');
          }
        }
      )*/
      this.options.series[0].setData(this.opciones), true;
      this.spline.series[0].setData(this.opcionesTSP), true;
      this.torta.series[0].setData(this.opcionesT), true;
      
      /*this.options.update({
        xAxis: [{
          type: 'datetime',
          min: ,
          max: 
        }]
      })*/ 
    }
    this.FeForm.reset();
    this.submitted = false;
  }
}