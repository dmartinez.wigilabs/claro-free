import { Component, OnInit } from '@angular/core';
import { ApiService } from '../api.service';
import { Router } from '@angular/router';
import { ActivatedRoute } from '@angular/router';  
import { OwlDateTimeModule, OwlNativeDateTimeModule, OwlDateTimeIntl} from 'ng-pick-datetime';
import { FormControl, FormGroup, Validators, FormBuilder  } from '@angular/forms';
import { DateTimeAdapter } from 'ng-pick-datetime';
import { formatDate } from '@angular/common';
import { DatePipe } from '@angular/common';
import { NgxSpinnerService } from 'ngx-spinner';
import MapModule from 'highcharts/modules/map';


@Component({
  selector: 'app-dashboard',
  templateUrl: './dashboard.component.html',
  styleUrls: ['./dashboard.component.css']
})
export class DashboardComponent implements OnInit {
  typeInput: boolean;
  public datos = [];
  public Info = [];
  public Apps = "apps"
  public appName = "0"
  public allApps:any;
  public AppID: any;
  public Copiafechas
  lat: number = 4.698830;
  lng: number = -74.040987;
  public myDate = new Date();
  public fechaAct = new Date()
  public fechaAc = new Date().getTime();
  public DIA_EN_MILISEGUNDOS = 24 * 60 * 60 * 1000;
  public ayer = new Date(this.fechaAc - 24*60*60*1000);
  public newAc =  this.datePipe.transform(new Date(this.fechaAct),"yyyy-MM-dd"); 

  constructor(private apiService : ApiService, private router : Router, private route: ActivatedRoute,  private formBuilder: FormBuilder, private datePipe: DatePipe, dateTimeAdapter: DateTimeAdapter<any>,
    private spinner: NgxSpinnerService) {}

  ngOnInit() {
  console.log(this.fechaAc);
  this.apiService.postFechas(this.ayer, this.newAc, this.Apps ).subscribe((data) => {
        if (data.error == 1) {
          alert('error');     
        }else{
          console.log('succes');
          this.datos = data.response; 
          console.log(this.datos);  
        }
      }
    )
    console.log(this.ayer); 
  };
  addType() {
    this.typeInput = !this.typeInput;
  }
  Logout(){
    this.apiService.Logout();
    this.router.navigate(['/inicio'])
  }
}
