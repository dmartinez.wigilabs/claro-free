import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { DashboardComponent } from './dashboard/dashboard.component';
import { TodasComponent } from './todas/todas.component';
import { RappiComponent } from './rappi/rappi.component';
import { InicioComponent } from './inicio/inicio.component';
import { HighchartsChartComponent } from 'highcharts-angular';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { ReactiveFormsModule } from '@angular/forms';
import { AgmCoreModule } from '@agm/core';
import { OwlDateTimeModule, OwlNativeDateTimeModule } from 'ng-pick-datetime';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';
import { DatePipe } from '@angular/common';
import { NgxSpinnerModule } from 'ngx-spinner';
import { AuthGuard } from './authGuard/auth.guard';
import * as highmaps from 'highcharts/modules/map.src';
import { VistaAppsComponent } from './vista-apps/vista-apps.component';



@NgModule({
  declarations: [
    AppComponent,
    DashboardComponent,
    TodasComponent,
    RappiComponent,
    InicioComponent,
    VistaAppsComponent,  
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    AgmCoreModule.forRoot({
      apiKey: 'AIzaSyCeUZ4BzsoVCSMnnGdNUQxDlIMSxEQhmLU',
      libraries: ['place']
    }),
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
    BrowserAnimationsModule,
    NgxSpinnerModule
  ],
  providers: [
    {provide: OWL_DATE_TIME_LOCALE, useValue: 'COT'},
    DatePipe,
    AuthGuard,  
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }
