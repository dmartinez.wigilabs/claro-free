import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders, HttpResponse  } from '@angular/common/http';
import { BehaviorSubject, Observable } from 'rxjs';
import { Rappi } from './rappi/rappi'
import { Todas } from './todas/todas'
import { map } from 'rxjs/operators';
import { LoginI } from './inicio/login-i'


const httpOptions = {
  headers: new HttpHeaders({
    'Content-Type':  'application/x-www-form-urlencoded'
  })};

@Injectable({
  providedIn: 'root'
})
export class ApiService {

  apiURl: string = 'http://miclarodev-api-miclarodev.104.209.147.150.nip.io/M3/Prepago/claroFreeDash/';

  extractData(res: Response) {
    let body = res;
    return body || {};
  } 
  
  constructor(private http : HttpClient) { }
  
  Logout (): void {
    localStorage.setItem ('isLoggedIn','false');
    //localStorage.removeItem('token');
  }

  postFechas(ayer : any, newAc : any, Apps : any): Observable<any>{   
    return this.http.post<any>(this.apiURl, 
      {
        data:{
          fIni: ayer,
          fFin: newAc,
          tipo: Apps,  
        }
      });
    } 
    postGetInfo(ayer : any, newAc : any, Apps : any, name: any): Observable<any>{   
      return this.http.post<any>(this.apiURl, 
        {
          data:{
            fIni: ayer,
            fFin: newAc,
            tipo: Apps, 
            appName: name
          }
        });
      } 
  /*loginUsuarios(correo: any, password: any): Observable<any>{
    return this.http.post(this.apiURl + "login",
    {
      data:{
        correo: correo,
        clave: password
      }
    }).pipe(
      map(this.extractData)
    )

  }*/
  public getFechas(url?: string){
      return this.http.get(`${this.apiURl}/getAliados.php/`);
  }
  
}
